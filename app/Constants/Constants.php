<?php


namespace App\Constants;


class Constants
{
    const NOT_FOUND_ERROR = 'پیدا نشد';
    const PASSWORD_ERROR = 'پسورد صحیح نیست';
    const ACCESS_DENIED_ERROR = 'شما به این بخش دسترسی ندارید';
}
