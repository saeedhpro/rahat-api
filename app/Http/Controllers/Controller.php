<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function createError(string $name, string $error, int $status) {
        return response()->json(['errors' => [ $name => [$error]]], $status);
    }

    public function hasPage(): bool
    {
        return \request()->has('page');
    }

    protected function getPage() {
        $page = \request()->get('page');
        return is_null($page) ? 10 : $page;
    }

    protected function getLimit() {
        $limit = \request()->get('limit');
        return is_null($limit) ? 10 : $limit;
    }
}
