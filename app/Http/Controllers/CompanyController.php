<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Resources\CompanyCollectionResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\UserCollectionResource;
use App\Interfaces\CompanyInterface;
use App\Interfaces\UserInterface;

class CompanyController extends Controller
{
    private $companyRepository;
    private $userRepository;

    public function __construct(CompanyInterface $companyRepository, UserInterface $userRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
    }

    public function index(): CompanyCollectionResource
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        return new CompanyCollectionResource(
            $this->companyRepository->allByPaginate($page, $limit)
        );
    }

    public function store(CompanyStoreRequest $request): CompanyResource
    {
        $company = $this->companyRepository->create($request->only([
            'name',
            'email',
            'website',
            'logo',
        ]));
        return new CompanyResource($company);
    }

    public function show(int $id): CompanyResource
    {
        return new CompanyResource(
            $this->companyRepository->show($id)
        );
    }

    public function update(CompanyUpdateRequest $request, int $id)
    {
        return $this->companyRepository->update($request->only([
            'name',
            'email',
            'website',
			'logo'
        ]), $id);
    }

    public function employees(int $id)
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        return new UserCollectionResource(
            $this->userRepository->findByWithPagination(
                [
                    'company_id' => $id
                ],
                [],
                'created_at',
                'desc',
                $page,
                $limit
            )
        );
    }

    public function comment(CommentStoreRequest $request, int $id)
    {
        return $this->companyRepository->comment($request->only([
            'body'
        ]), $id);
    }

    public function comments($id)
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        return $this->companyRepository->comments($id, $page, $limit);
    }
}
