<?php

namespace App\Http\Controllers;

use App\Constants\Constants;
use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserCollectionResource;
use App\Http\Resources\UserLoginResource;
use App\Http\Resources\UserResource;
use App\Interfaces\UserInterface;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(UserLoginRequest $request)
    {
        /** @var User $user */
        $user = $this->userRepository->login($request);
        if($this->checkPassword($request->get('password'), $user->password)) {
            $token = $user->createToken('rahat')->accessToken;
            return new UserLoginResource($user, $token);
        } else {
            return $this->createError('password', Constants::PASSWORD_ERROR, 422);
        }
    }

    private function checkPassword($password, $userPassword): bool
    {
        return Hash::check($password, $userPassword);
    }

    public function index(): UserCollectionResource
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        return new UserCollectionResource(
            $this->userRepository->allByPaginate($page, $limit)
        );
    }

    public function store(UserStoreRequest $request): UserResource
    {
        $request['password'] = bcrypt($request->get('password'));
        $user = $this->userRepository->create($request->only([
            'name',
            'family',
            'phone_number',
            'company_id',
            'email',
            'password',
        ]));
        return new UserResource($user);
    }

    public function show(int $id): UserResource
    {
        return new UserResource(
            $this->userRepository->show($id)
        );
    }

    public function update(UserUpdateRequest $request, int $id): JsonResponse
    {
        /** @var User $auth */
        $auth = auth()->user();
        if($auth->can('update-user', $id)) {
            return $this->userRepository->update($request->only([
                'name',
                'family',
                'phone_number',
                'company_id',
                'email',
            ]), $id);
        } else {
            return $this->createError('user', Constants::ACCESS_DENIED_ERROR, 403);
        }
    }

    public function comment(CommentStoreRequest $request, int $id)
    {
        return $this->userRepository->comment($request->only([
            'body'
        ]), $id);
    }

    public function comments($id)
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        return $this->userRepository->comments($id, $page, $limit);
    }
}
