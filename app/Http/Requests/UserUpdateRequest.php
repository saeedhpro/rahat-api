<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$id = $this->user()->id;
        return [
            'email' => 'required|email|unique:users,id,'. $id,
            'name' => 'required|string|min:3',
            'family' => 'required|string|min:3',
            'phone_number' => 'required|phone:IR|unique:users,id,'. $id,
            'company_id' => 'required|exists:companies,id',
        ];
    }
}
