<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'family' => $this->family,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'company' => new CompanySimpleResource($this->company),
            'last_comments' => $this->comments()->orderByDesc('created_at')->get()->take(2),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
