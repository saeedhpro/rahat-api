<?php

namespace App\Providers;

use App\Interfaces\CompanyInterface;
use App\Repositories\CompanyRepository;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\BaseInterface;
use App\Interfaces\UserInterface;
use App\Repositories\BaseRepository;
use App\Repositories\UserRepository;
use App\Models\Company;
use App\Models\User;

class BackendServiceProvider extends ServiceProvider {

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            BaseInterface::class,
            BaseRepository::class
        );
        $this->app->bind(
            UserInterface::class,
            function() {
                return new UserRepository(new User);
            }
        );
        $this->app->bind(
            CompanyInterface::class,
            function() {
                return new CompanyRepository(new Company);
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
