<?php

namespace App\Repositories;

use App\Interfaces\BaseInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 *
 * @package \App\Repositories
 */
class BaseRepository implements BaseInterface{

    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Create a model instance
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Update a model instance
     * @param array $attributes
     * @param int $id
     * @return mixed
     */
    public function update(array $attributes, int $id)
    {
        return $this->findOneOrFail($id)->update($attributes);
    }

    /**
     * Return all model rows
     * @param array $columns
     * @param string[] $with
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = array('*'), $with = array(), $orderBy = 'desc', $sortBy = 'id')
    {
        return $this->model->with($with)->orderBy($orderBy, $sortBy)->get($columns);
    }

    /**
     * Return all model rows
     * @param array $columns
     * @param string[] $with
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function allByPagination($columns = array('*'), $with = array(), $orderBy = 'id', $sortBy = 'desc', $page = 1, $limit = 10)
    {
        return $this->model->with($with)->orderBy($orderBy, $sortBy)->paginate($limit);
    }
        /**
     * Find one by ID
     * @param int $id
     * @param string[] $with
     * @return mixed
     */
    public function find(int $id, $with = array()) {
        return $this->model->with($with)->find($id);
    }

    /**
     * Find one by ID or throw exception
     * @param int $id
     * @param string[] $with
     * @return mixed
     */
    public function findOneOrFail(int $id, $with = array()) {
        return $this->model->with($with)->findOrFail($id);
    }

    /**
     * Find based on a different column
     * @param array $data
     * @param string[] $with
     * @return mixed
     */
    public function findBy(array $data, $with = array()) {
        return $this->model->with($with)->where($data)->get();
    }

    /**
     * Find based on a different column
     * @param array $data
     * @param string[] $with
     * @return mixed
     */
    public function findByWithPagination(array $data, $with = array(), $orderBy = 'id', $sortBy = 'desc', $page = 1, $limit = 10) {
        return $this->model->with($with)->where($data)->orderBy($orderBy, $sortBy)->paginate($limit);
    }

    /**
     * Find based on a different column
     * @param array $data
     * @param string[] $with
     * @return mixed
     */
    public function findOneBy(array $data, $with = array()) {
        return $this->model->with($with)->where($data)->first();
    }

    /**
     * Find one based on a different column or through exception
     * @param array $data
     * @return mixed
     */
    public function findOneByOrFail(array $data, $with = array())
    {
        return $this->model->with($with)->where($data)->firstOrFail();
    }

    /**
     * Delete one by Id
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->findOneOrFail($id)->delete();
    }
}
