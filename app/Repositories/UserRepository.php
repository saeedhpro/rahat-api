<?php

namespace App\Repositories;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Interfaces\UserInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRepository
 *
 * @package \App\Repositories
 */
class UserRepository extends BaseRepository implements UserInterface{

    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function login(UserLoginRequest $request)
    {
        return $this->findOneByOrFail(
            ['email' => $request->get('email')],
            ['company']
        );
    }

    public function allByPaginate($page = 1, $limit = 10)
    {
        return $this->allByPagination('*', ['company'],
            'created_at', 'desc',
            $page, $limit);
    }

    public function show(int $id)
    {
        return $this->findOneByOrFail(
            ['id' => $id],
            ['company']
        );
    }

    public function comment(array $attributes, int $id)
    {
        /** @var User $user */
        $user = $this->findOneOrFail($id);
        $attributes['user_id'] = auth()->user()->id;
        return $user->comments()->create($attributes);
    }

    public function comments(int $id, int $page, int $limit)
    {
        /** @var User $user */
        $user = $this->findOneOrFail($id);
        return $user->comments()->paginate($limit);
    }
}
