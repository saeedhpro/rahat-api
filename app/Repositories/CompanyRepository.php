<?php

namespace App\Repositories;

use App\Interfaces\CompanyInterface;
use App\Interfaces\UserInterface;
use App\Models\Company;
use Illuminate\Database\Eloquent\Model;
/**
 * Class CompanyRepository
 *
 * @package \App\Repositories
 */
class CompanyRepository extends BaseRepository implements CompanyInterface {

    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function employees(int $companyId, int $page = 1, int $limit = 10)
    {
        // TODO: Implement employees() method.
    }

    public function allByPaginate($page = 1, $limit = 10)
    {
        return $this->allByPagination('*', [],
            'created_at', 'desc',
            $page, $limit);
    }

    public function show(int $id)
    {
        return $this->findOneByOrFail(
            ['id' => $id],
        );
    }

    public function comment(array $attributes, int $id)
    {
        /** @var Company $company */
        $company = $this->findOneOrFail($id);
        $attributes['user_id'] = auth()->user()->id;
        return $company->comments()->create($attributes);
    }

    public function comments(int $id, int $page, int $limit)
    {
        /** @var Company $company */
        $company = $this->findOneOrFail($id);
        return $company->comments()->paginate($limit);
    }
}
