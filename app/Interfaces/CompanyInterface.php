<?php

namespace App\Interfaces;

/**
 * Interface CompanyInterface
 * @package App\Interfaces
 */
interface CompanyInterface extends BaseInterface {

    public function employees(int $companyId, int $page = 1, int $limit = 10);
    public function allByPaginate($page = 1, $limit = 10);
    public function show(int $id);
    public function comment(array $attributes, int $id);
    public function comments(int $id, int $page, int $limit);
}
