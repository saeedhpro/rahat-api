<?php

namespace App\Interfaces;

/**
 * Interface BaseInterface
 * @package App\Interfaces
 */
interface BaseInterface {

    /**
     * Create a model instance
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Update a model instance
     * @param array $attributes
     * @param int $id
     * @return mixed
     */
    public function update(array $attributes, int $id);

    /**
     * Return all model rows
     * @param string[] $columns
     * @param string[] $with
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = array('*'), $with = array(), $orderBy = 'desc', $sortBy = 'id');

    /**
     * Return all model rows by paginate
     * @param string[] $columns
     * @param string[] $with
     * @param string $orderBy
     * @param string $sortBy
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function allByPagination($columns = array('*'), $with = array(), $orderBy = 'id', $sortBy = 'desc', $page = 1, $limit = 10);

    /**
     * Find one by ID
     * @param int $id
     * @param string[] $with
     * @return mixed
     */
    public function find(int $id, $with = array());

    /**
     * Find one by ID or throw exception
     * @param int $id
     * @param string[] $with
     * @return mixed
     */
    public function findOneOrFail(int $id, $with = array());

    /**
     * Find based on a different column
     * @param array $data
     * @param string[] $with
     * @return mixed
     */
    public function findBy(array $data, $with = array());

    /**
     * Find based on a different column
     * @param array $data
     * @param string[] $with
     * @return mixed
     */
    public function findByWithPagination(array $data, $with = array(), $orderBy = 'id', $sortBy = 'desc', $page = 1, $limit = 10);

    /**
     * Find based on a different column
     * @param array $data
     * @param string[] $with
     * @return mixed
     */
    public function findOneBy(array $data, $with = array());

    /**
     * Find one based on a different column or through exception
     * @param array $data
     * @return mixed
     */
    public function findOneByOrFail(array $data, $with = array());

    /**
     * Delete one by Id
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);
}
