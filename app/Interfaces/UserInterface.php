<?php

namespace App\Interfaces;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserUpdateRequest;

/**
 * Interface UserInterface
 * @package App\Interfaces
 */
interface UserInterface extends BaseInterface {
    public function login(UserLoginRequest $request);
    public function allByPaginate($page = 1, $limit = 10);
    public function show(int $id);
    public function comment(array $attributes, int $id);
    public function comments(int $id, int $page, int $limit);
}
