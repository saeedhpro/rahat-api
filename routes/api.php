<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/auth')->group(function () {
    Route::post('/login', [UserController::class, 'login'])->name('auth.login');
});

Route::prefix('/users')->middleware('auth:api')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('users.index');
    Route::post('/', [UserController::class, 'store'])->name('users.store');
    Route::get('/{id}', [UserController::class, 'show'])->name('users.show');
    Route::put('/{id}', [UserController::class, 'update'])->name('users.update');
    Route::post('/{id}/comment', [UserController::class, 'comment'])->name('users.comments.store');
    Route::get('/{id}/comments', [UserController::class, 'comments'])->name('users.comments.index');
});

Route::prefix('/companies')->middleware('auth:api')->group(function () {
    Route::get('/', [CompanyController::class, 'index'])->name('companies.index');
    Route::post('/', [CompanyController::class, 'store'])->name('companies.store');
    Route::get('/{id}', [CompanyController::class, 'show'])->name('companies.show');
    Route::put('/{id}', [CompanyController::class, 'update'])->name('companies.update');
    Route::get('/{id}/employees', [CompanyController::class, 'employees'])->name('companies.users.index');
    Route::post('/{id}/comment', [CompanyController::class, 'comment'])->name('companies.comments.store');
    Route::get('/{id}/comments', [CompanyController::class, 'comments'])->name('companies.comments.index');
});

Route::prefix('/upload')->middleware('auth:api')->group(function () {
    Route::post('/', [UploadController::class, 'upload'])->name('upload.image');
});
